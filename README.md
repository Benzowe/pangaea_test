# Pangaea-Test

## Instructions for setup

1. add `.env` file into root directory
2. use `.env.sample` to fill in your environment variables by copying to your `.env`
3. add your credentials into the `.env` file
4. to start the application, `sh ./start-server.sh`

## Creating a Subscription
- ` curl -X POST -d '{ "url": "http://localhost:8000/event"}' http://localhost:8000/subscribe/topic1`

### To publish an event
- ` curl -X POST -H "Content-Type: application/json" -d '{"message": "hello"}' http://localhost:8000/publish/topic1`