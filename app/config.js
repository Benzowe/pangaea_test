require('dotenv').config();
const redis = require('redis');
const bluebird = require('bluebird');
const axios = require('axios');


const redisConfig = {
  redisHost: process.env.REDIS_HOST,
  redisPort: process.env.REDIS_PORT,
  redisPassword: process.env.REDIS_PASSWORD
}

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const client = redis.createClient(redisConfig);

const post = (slug, data) => {
  return axios({
    method: 'post',
    url: slug,
    data
  })
}

module.exports = { client, post };