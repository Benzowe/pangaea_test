const express = require('express');
const routes = require('./routes');
const parser = require('body-parser');
const app = express();
const port = process.env.PORT || 8000;

app.use(parser.urlencoded({ extended: true }));
app.use(parser.json());
routes.r(app);
app.listen(port, () => {
  console.log(`Magic Happens on http://localhost:${port}`);
})