const { success, failure } = require('./utils/response_manager');
const HTTPStatus = require('./utils/http_status');
const { client, post } = require('./config');

client.on('connect', () => {
  console.log('redis client connected successfully')
});
client.on('error', (err) => {
  console.log('Error connecting to redis' + err);
});

module.exports.r = (app) => {

  app.get('/', async (req, res) => {
    return success(res, {
      message: 'WELCOME TO BASE_URL',
    }, HTTPStatus.OK);
  });

  /**
   * Subscribing to a topic
   * @param: topic
   * @param: data
   */
  app.post('/subscribe/:topic', async (req, res) => {
    const { topic } = req.params;
    let data = req.body;
    if (!topic) {
      return failure(res, { message: 'Please provide a topic in url!' },
        HTTPStatus.BAD_REQUEST);
    }
    if (!data) {
      return failure(res, { message: 'Please provide a request body' },
        HTTPStatus.BAD_REQUEST);
    }
    try {
      data = JSON.parse(Object.keys(data)[0]);
      const setSubscription = await client.setAsync(topic, JSON.stringify(data));
      if (setSubscription) {
        return success(res, {
          message: 'Subscription Added Successfully',
        }, HTTPStatus.OK);
      }
      else {
        return failure(res, { message: 'Subscription could not be created' },
          HTTPStatus.INTERNAL_SERVER_ERROR);
      }
    } catch (error) {
      return failure(res, { message: 'An error Occured ', error },
        HTTPStatus.INTERNAL_SERVER_ERROR);
    }
  })

  /**
 * Publishing a topic
 * @param: topic
 * @param: data
 * @returns 201
 */
  app.post('/publish/:topic', async (req, res) => {
    const { topic } = req.params;
    let data = req.body;
    if (!topic) {
      return failure(res, { message: 'Please provide a topic in url!' },
        HTTPStatus.BAD_REQUEST);
    }
    if (!data) {
      return failure(res, { message: 'Please provide a request body' },
        HTTPStatus.BAD_REQUEST);
    }
    try {
      const getTopic = await client.getAsync(topic);
      if (getTopic) {
        const subscriptionBody = JSON.parse(getTopic);

        data = JSON.stringify(data);
        const param = {
          topic,
          body: data
        }

        const result = post(`${subscriptionBody.url}`, param);
        if (result) {
          return success(res, {
            message: 'Publish Event Triggered Successfully',
          }, HTTPStatus.CREATED);
        }
        else {
          return failure(res, { message: 'Could not trigger event' },
            HTTPStatus.INTERNAL_SERVER_ERROR);
        }
      }
      else {
        return failure(res, { message: 'No topic Found' },
          HTTPStatus.NOT_FOUND);
      }
    } catch (error) {
      return failure(res, { message: 'An error Occured ', error },
        HTTPStatus.INTERNAL_SERVER_ERROR);
    }
  })

  app.post('/event', (req, res) => {
    const data = req.body
    console.log('topic Name =', data.topic)
    console.log('body Body =', data.body)
    return success(res, {
      message: 'Data returned Successfully',
      response: data
    }, HTTPStatus.CREATED);
  })
}